let app = require('express')();
let http = require('http').Server(app);
let io = require('socket.io')(http);

io.on('connection', (socket) => {
  console.log("User connected");
  
  socket.on('disconnect', function(){
    io.emit('users-changed', {user: socket.nickname, event: 'dejó el roto'}); 
  });
 
  socket.on('set-nickname', (nickname) => {
    socket.nickname = nickname;
    console.log("User "+nickname+" in da house");
    io.emit('users-changed', {user: nickname, event: '[en crescendo] issss, arrimó'}); 
  });
  
  socket.on('add-message', (message) => {
    io.emit('message', {text: message.text, from: socket.nickname, created: new Date()});    
  });

});
 
var port = 3001;
 
http.listen(port, function(){
   console.log('listening in http://172.16.114.27:' + port);
});
